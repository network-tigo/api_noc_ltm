echo "host" $1
echo "path" $2
java -server -XX:+UseParallelGC -DlocalRmiRegistryPort=9002 -Dcom.sun.management.jmxremote=true -Dcom.sun.management.jmxremote.port=9102 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Djava.rmi.server.hostname=$1 -jar $2/api_contact_lite.jar --server.port=9082 & echo $! > $2/pid.file &