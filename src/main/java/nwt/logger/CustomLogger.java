package nwt.logger;

import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CustomLogger {
	
	@Value("${logger.maxpayloadlength}")
	private int maxPayloadLength;
	
	@Value("${logger.logpayloadstatusok}")
	private boolean logPayLoadStatusOk;

	private boolean includeResponsePayload = true;
	
	Logger loggerInfo = LoggerFactory.getLogger("CustomLoggerInfo");
	Logger loggerError = LoggerFactory.getLogger("CustomLoggerError");
	
	private String getContentAsString(byte[] buf, int maxLength, String charsetName) {
		if (buf == null || buf.length == 0)
			return "";
		int length = Math.min(buf.length, this.maxPayloadLength);
		try {
			return new String(buf, 0, length, charsetName);
		} catch (UnsupportedEncodingException ex) {
			return "Unsupported Encoding";
		}
	}
	
	public void WriteToLog(ContentCachingRequestWrapper request, ContentCachingResponseWrapper response, long startTime, String error) throws JSONException {
		ContentCachingRequestWrapper wrappedRequest = new ContentCachingRequestWrapper(request);		
		String requestBody = this.getContentAsString(wrappedRequest.getContentAsByteArray(), this.maxPayloadLength,
				request.getCharacterEncoding());		
			
		String responseBody="";
		if(error !=null) {
			responseBody = error;
		}
		else if (includeResponsePayload) {
			byte[] buf = response.getContentAsByteArray();
			responseBody= getContentAsString(buf, this.maxPayloadLength, response.getCharacterEncoding());			
		}

		
		long duration = System.currentTimeMillis() - startTime;
		
		JSONObject logJson= new JSONObject();
		logJson.put("name", "APICONTACT");
		logJson.put("uri", request.getRequestURL());
		logJson.put("responseCode", response.getStatus());
		logJson.put("responseTime", duration);
		logJson.put("req", requestBody);
		String clientIp=request.getHeader("X-Forwarded-For");
		logJson.put("clientIp", clientIp);	
		try {
			logJson.put("hostName", InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			logJson.put("hostName", wrappedRequest.getLocalAddr() + ":" + wrappedRequest.getLocalPort());
		}
		RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();

        String jvmName = bean.getName();
        long pid = Long.valueOf(jvmName.split("@")[0]);
		logJson.put("pid", pid);
		
		logJson.put("level",0);
		if(logPayLoadStatusOk==true ||  response.getStatus() != 200) {			
			logJson.put("msg", responseBody);			
		}		
		logJson.put("time",  new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(startTime));
		if(response.getStatus()==200 || response.getStatus()==201 || response.getStatus()==204) {
			loggerInfo.info(logJson.toString());			
		}
		else {
			if (response.getStatus()==400 || response.getStatus()==500 || response.getStatus()==401 || response.getStatus()==403) {
				try
				{
					JSONObject errResponse = new JSONObject(responseBody);
					if (errResponse.has("error")) {
						logJson.put("msg", errResponse.getJSONObject("error").getString("description"));							
					}
				}
				catch(Exception ex) {}
			}
			else if( response.getStatus()==404) {
				logJson.put("msg", "not found");
			}
			loggerError.error(logJson.toString());
		}
	}
}
