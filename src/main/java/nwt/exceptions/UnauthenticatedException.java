package nwt.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class UnauthenticatedException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public UnauthenticatedException(String exception) {
		super(exception);
	}
	
}
