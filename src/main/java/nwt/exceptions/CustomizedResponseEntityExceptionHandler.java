package nwt.exceptions;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.exc.InvalidDefinitionException;

import nwt.exceptions.BadRequestException;
import nwt.exceptions.ForbiddenException;
import nwt.exceptions.InternalServerErrorException;
import nwt.exceptions.NotFoundException;
import nwt.exceptions.ServiceUnavailableException;
import nwt.exceptions.UnauthorizedException;
import nwt.mongo.Error;
import nwt.mongo.ErrorResponse;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Autowired
	private MessageSource messageSource;
	
	private static final Locale lang = new Locale("en");
	
	@ExceptionHandler(BadRequestException.class)
	public final ResponseEntity<ErrorResponse> handleBadRequestException(BadRequestException ex, WebRequest request) {
		Error error = new Error("MSG", messageSource.getMessage("bad_request", new Object[] {ex.getMessage()}, lang), "005", 400l);
		ErrorResponse er = new ErrorResponse(error);
		return new ResponseEntity<>(er, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(InternalServerErrorException.class)
	public final ResponseEntity<ErrorResponse> handleInternalServerErrorException(InternalServerErrorException ex, WebRequest request) {
		Error error = new Error("COM", messageSource.getMessage("internal_server_error", new Object[] {}, lang), "003", 500l);
		ErrorResponse er = new ErrorResponse(error);
		return new ResponseEntity<>(er, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(UnauthorizedException.class)
	public final ResponseEntity<ErrorResponse> handleUnauthorizedException(UnauthorizedException ex, WebRequest request) {
		Error error = new Error("NEG", messageSource.getMessage("invalid_organization", new Object[] {ex.getMessage()}, lang), "006", 401l);
		ErrorResponse er = new ErrorResponse(error);
		return new ResponseEntity<>(er, HttpStatus.UNAUTHORIZED);
	}
	
	@ExceptionHandler(NotFoundException.class)
	public final ResponseEntity<ErrorResponse> handleNotFoundException(NotFoundException ex, WebRequest request) {
		Error error = new Error("MSJ", messageSource.getMessage("not_found", new Object[] {}, lang), "004", 404l);
		ErrorResponse er = new ErrorResponse(error);
		return new ResponseEntity<>(er, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(ServiceUnavailableException.class)
	public final ResponseEntity<ErrorResponse> handleServiceUnavailableException(ServiceUnavailableException ex, WebRequest request) {
		Error error = new Error("COM", messageSource.getMessage(ex.getMessage(), new Object[] {}, lang), "001", 503l);
		ErrorResponse er = new ErrorResponse(error);
		return new ResponseEntity<>(er, HttpStatus.SERVICE_UNAVAILABLE);
	}
	
	@Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		Error error = new Error("MSG", messageSource.getMessage("bad_request", new Object[] {"Error en tipo de dato de entrada"}, lang), "005", 400l);
		ErrorResponse er = new ErrorResponse(error);
		return new ResponseEntity<>(er, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler({MethodArgumentTypeMismatchException.class, InvalidDefinitionException.class})
	public final ResponseEntity<ErrorResponse> handleMethodArgumentTypeMismatchException(Exception ex, WebRequest request) {
		Error error = new Error("MSG", messageSource.getMessage("bad_request", new Object[] {"Error en tipo de dato de entrada"}, lang), "005", 400l);
		ErrorResponse er = new ErrorResponse(error);
		return new ResponseEntity<>(er, HttpStatus.BAD_REQUEST);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		Error error = new Error("MSG", messageSource.getMessage("bad_request", new Object[] {"Error en tipo de dato de entrada"}, lang), "005", 400l);
		ErrorResponse er = new ErrorResponse(error);
		return new ResponseEntity<>(er, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(UnauthenticatedException.class)
	public final ResponseEntity<ErrorResponse> handleUnauthenticatedException(UnauthenticatedException ex, WebRequest request) {
		Error error = new Error("SEG", messageSource.getMessage(ex.getMessage(), new Object[] {}, lang), "006", 401l);
		ErrorResponse er = new ErrorResponse(error);
		return new ResponseEntity<>(er, HttpStatus.UNAUTHORIZED);
	}
	
	@ExceptionHandler(ForbiddenException.class)
	public final ResponseEntity<ErrorResponse> handleBadRequestException(ForbiddenException ex, WebRequest request) {
		Error error = new Error("MSJ", messageSource.getMessage("forbiden", new Object[] {ex.getMessage()}, lang), "007", 403l);
		ErrorResponse er = new ErrorResponse(error);
		return new ResponseEntity<>(er, HttpStatus.FORBIDDEN);
	}
}
