package nwt.mongo;

import java.io.Serializable;

public class Error implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String errorType;
	private String description;
	private String code;
	private Long errorCode;
	
	public Error(String errorType, String description, String code, Long errorCode) {
		super();
		this.errorType = errorType;
		this.description = description;
		this.code = code;
		this.errorCode = errorCode;
	}
	
	public String getErrorType() {
		return errorType;
	}
	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Long getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Long errorCode) {
		this.errorCode = errorCode;
	}

}
