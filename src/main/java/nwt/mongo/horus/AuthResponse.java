package nwt.mongo.horus;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthResponse {
	@JsonProperty("username")
	private String userName;
	private String name;
	private ArrayList<String> permissions;
	private String token;
	private String photo;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<String> getPermissions() {
		return permissions;
	}
	public void setPermissions(ArrayList<String> permissions) {
		this.permissions = permissions;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
