package nwt.mongo;

import java.io.Serializable;

public class ErrorResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Error error;
	
	public ErrorResponse() {}
	public ErrorResponse(Error error) {
		this.error = error;
	}

	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}

}
