package nwt.repositories.impala;

import org.springframework.stereotype.Repository;


import nwt.impala.Sites;
import nwt.impala.SitesDay;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface SitesInfoRepo extends CrudRepository<SitesDay,Sites> {
	@Query(value="SELECT DISTINCT esn, devicename, ecgi, collecttime, ipaddress, imsi, ip   "
			+ " FROM (SELECT  prttn_dt,esn, devicename, ecgi,max(collecttime) collecttime, ipaddress, imsi , ip "
			+ " from stage.st_ltm_wttx  "
			+ " where prttn_dt>= cast(from_timestamp(days_add(now(),-3), 'yyyyMMdd')  as integer) "
			+ " and devicename = ?1 group by prttn_dt,esn, devicename, ecgi, ipaddress, imsi, ip ) X ",
			nativeQuery = true)
	List<SitesDay> CgisbyDeviceName(String devicename);
}