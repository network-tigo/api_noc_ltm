package nwt.repositories.impala;

import org.springframework.stereotype.Repository;

import nwt.impala.LTM;
import nwt.impala.LTMDay;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface LTMDayInfoRepo extends CrudRepository<LTMDay,LTM> {
	
	
	@Query(value="select prttn_dt,esn, devicename, softwareversion, modelname,   "
			+ " ip, sinr, rsrp, rsrq, rssi, cqi,band, tac,power, totalupload, "
			+ " totaldownload, ecgi, maxdlthroughput, maxulthroughput, totalusernum, imsi, iccid, collecttime,ipaddress from ( "	
			+ " SELECT prttn_dt,esn, devicename, softwareversion, modelname, "
			+ " ip, sinr, rsrp, rsrq, rssi, cqi,band, tac,power, totalupload, "
			+ " totaldownload, ecgi, maxdlthroughput, maxulthroughput, totalusernum, imsi, iccid, collecttime,ipaddress FROM"
			+ " stage.st_ltm_wttx WHERE prttn_dt >= cast(from_timestamp(days_add(now(),-5), 'yyyyMMdd')  as integer) ) h"
			+ " where devicename =?1",
			nativeQuery = true)
	List<LTMDay> findByDeviceName(String devicename);
	
	
	

}

