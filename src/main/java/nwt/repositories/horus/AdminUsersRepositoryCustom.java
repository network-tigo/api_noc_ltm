package nwt.repositories.horus;

import nwt.mongo.horus.AdminUser;

public interface AdminUsersRepositoryCustom {
	AdminUser getUserConsideringId(String id);
}
