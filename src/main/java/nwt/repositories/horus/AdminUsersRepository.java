package nwt.repositories.horus;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import nwt.mongo.horus.AdminUser;

@Repository
public interface AdminUsersRepository extends MongoRepository<AdminUser, String>, AdminUsersRepositoryCustom{	

}
