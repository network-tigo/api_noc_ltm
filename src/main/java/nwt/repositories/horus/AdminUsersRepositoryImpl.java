package nwt.repositories.horus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;

import nwt.mongo.horus.AdminUser;

import org.springframework.data.mongodb.core.query.Criteria;

public class AdminUsersRepositoryImpl implements AdminUsersRepositoryCustom{

	@Autowired
    private MongoTemplate mongoTemplate;
	
	@Override
	public AdminUser getUserConsideringId(String id) {
		
		Query query = new Query(Criteria.where("_id").is(id));
		AdminUser result = mongoTemplate.findOne(query, AdminUser.class);
		return result;
	}

}
