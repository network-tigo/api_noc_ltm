package nwt.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(
		entityManagerFactoryRef ="clouderaEntityManager",
		transactionManagerRef="clouderaTransactionManager",
		basePackages = {"nwt.repositories.impala"}
)
public class ImpalaDBConfiguration {

}
