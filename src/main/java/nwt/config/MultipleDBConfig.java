package nwt.config;

import java.util.Collections;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;


@Configuration
public class MultipleDBConfig {
	
	
	@Primary
	@Bean(name="horus")
	@ConfigurationProperties(prefix="spring.data.mongodb")
	public MongoProperties getHorus() {
		return new MongoProperties();
	}
	
	@Bean(name="internet")
	@ConfigurationProperties(prefix="spring.seconddata.mongodb")
	public MongoProperties getInternet() {
		return new MongoProperties();
	}
	
	@Primary
	@Bean(name="horusTemplate")
	public MongoTemplate horusTemplate() throws Exception {
		return new MongoTemplate(horusFactory(getHorus()));
	}
	
	@Bean(name="internetTemplate")
	public MongoTemplate internetTemplate() throws Exception {
		return new MongoTemplate(internetFactory(getInternet()));
	}
	
	@Primary
	@Bean(name = "clouderaEntityManager")
    public LocalContainerEntityManagerFactoryBean getClouderaEntityManager(
    		EntityManagerFactoryBuilder builder,
    		@Qualifier("clouderaDataSource") DataSource clouderaDataSource){
		
        return builder
                .dataSource(clouderaDataSource)
                .packages("nwt.impala")
                .build();

    }
	
	@Bean(name="clouderaDataSourceProperties")
	@Primary
    @ConfigurationProperties(prefix="spring.data.cloudera")
    public DataSourceProperties clouderaDataSourceProperties(){
        return new DataSourceProperties();
    }
	
	@Bean(name="clouderaDataSource")
	@Primary
    @ConfigurationProperties(prefix="spring.cloudera")
    public com.cloudera.impala.jdbc.DataSource exadataDataSource(@Qualifier("clouderaDataSourceProperties") DataSourceProperties clouderaDataSourceProperties) {
		com.cloudera.impala.jdbc.DataSource ds = new com.cloudera.impala.jdbc.DataSource();
		ds.setURL(clouderaDataSourceProperties.getUrl()+"UID="+clouderaDataSourceProperties.getUsername()+";PWD="+clouderaDataSourceProperties.getPassword());
		return ds;
    }
	
	@Bean(name = "clouderaTransactionManager")
	@Primary
    public JpaTransactionManager transactionManager(@Qualifier("clouderaEntityManager") EntityManagerFactory clouderaEntityManager){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(clouderaEntityManager);
        return transactionManager;
    }
			
	@Bean
	@Primary
	public MongoDbFactory horusFactory(final MongoProperties mongo) throws Exception {
		return new SimpleMongoDbFactory(new MongoClientURI(mongo.getUri()));        
	}
	
	@Bean
	public MongoDbFactory internetFactory(final MongoProperties mongo) throws Exception {
		return new SimpleMongoDbFactory(new MongoClientURI(mongo.getUri()));
	}
}
