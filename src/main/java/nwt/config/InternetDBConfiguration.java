package nwt.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = 
"nwt.repositories.internet",
    mongoTemplateRef = "internetTemplate")
public class InternetDBConfiguration {

}
