package nwt.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.web.cors.CorsConfiguration;

import nwt.exceptions.UnauthenticatedException;
import nwt.mongo.GenericPrincipal;
import nwt.security.ApiAuthenticationEntryPoint;
import nwt.security.AuthenticationFilter;
import nwt.security.JwtTokenProvider;

@Configuration
@EnableWebSecurity
@Order(1)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	
	@Autowired
    private ApiAuthenticationEntryPoint unauthorizedHandler;	
	
	@Autowired
	private JwtTokenProvider jtp;
	
	@Autowired
	private MessageSource messageSource;
	
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		AuthenticationFilter filter = new AuthenticationFilter(messageSource);
		filter.setAuthenticationManager(new AuthenticationManager() {
			
			@Override
			public Authentication authenticate(Authentication authentication) throws UnauthenticatedException {
				GenericPrincipal auth = (GenericPrincipal) authentication.getPrincipal();
				
				if (auth == null) {
					throw new UnauthenticatedException("Client authentication error");
				}
				
				if(jtp.validateToken(auth.getToken()))
					authentication.setAuthenticated(true);
				else
					authentication.setAuthenticated(false);
				return authentication;
			}
		});
		
		CorsConfiguration ccConf=new CorsConfiguration();
		ccConf.addAllowedMethod(HttpMethod.GET);
		ccConf.addAllowedMethod(HttpMethod.POST);
		ccConf.addAllowedMethod(HttpMethod.DELETE);
		ccConf.applyPermitDefaultValues();
		
		httpSecurity.
			cors().configurationSource(request -> ccConf).and().
			antMatcher("/**").
			csrf().disable().
			exceptionHandling().authenticationEntryPoint(unauthorizedHandler).
			and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
			and().authorizeRequests().
			antMatchers("/auth-net-analytics/**").permitAll();	
		
		httpSecurity.
			antMatcher("/**").
			csrf().disable().
			exceptionHandling().authenticationEntryPoint(unauthorizedHandler).
			and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
			and().addFilter(filter).authorizeRequests().anyRequest().authenticated();
	}

}