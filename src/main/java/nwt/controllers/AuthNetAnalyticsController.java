package nwt.controllers;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.sql.Connection;
//agregado a pie
import java.sql.Driver; 

import javax.servlet.http.HttpServletRequest;
//agregado a pie
import javax.sql.DataSource;
import javax.sql.ConnectionPoolDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nwt.exceptions.BadRequestException;
import nwt.exceptions.InternalServerErrorException;
import nwt.exceptions.NoContentException;
import nwt.exceptions.ServiceUnavailableException;
import nwt.exceptions.UnauthenticatedException;
import nwt.mongo.horus.AuthRequest;
import nwt.mongo.horus.AuthResponse;
import nwt.services.CacheService;
import nwt.services.IAuthenticateService;

@RestController
public class AuthNetAnalyticsController {
	
	Logger logger = LoggerFactory.getLogger(AuthNetAnalyticsController.class);
	
	@Autowired
	private IAuthenticateService authService;
	
	@Autowired
	private CacheService cacheService;
	
	// Cache scheduled
	@Scheduled(fixedRate = 3600000)
	public void refresh() {
		cacheService.cleanCache();
	}		

	private void writeErrorToLogger(Exception e) {
        StringWriter sw= new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        Logger loggerInfo = LoggerFactory.getLogger(this.getClass());
        loggerInfo.error(sw.toString());
    }

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, value="/login")
	public ResponseEntity<AuthResponse> authenticate(
			@RequestBody AuthRequest requestInformation,
			HttpServletRequest request
	) {
		try {
			
			String username = requestInformation.getU();
			String password = requestInformation.getP();
						
			if(username == null || password == null) {
				throw new BadRequestException("campos requeridos");
			}
			
			// Call auth
			AuthResponse response = authService.authenticateUser(username, password);
			
			if (response == null) {
				throw new BadRequestException("Informacion no encontrada para " + username);
			}
			else {
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
		}
		catch(DataAccessException dae) {
			throw new ServiceUnavailableException();
		}
 		catch(BadRequestException bre) {
			throw new BadRequestException(bre.getMessage());
		}
		catch(UnauthenticatedException ue) {
			throw new UnauthenticatedException("Error de autenticacion de base de datos");
		}
		catch(NoContentException nce) {
			throw new NoContentException(nce.getMessage());
		}
		catch(Exception e) {
			throw new InternalServerErrorException();
		}
	}
}
