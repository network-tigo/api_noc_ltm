package nwt.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;


import nwt.impala.LTMDay;
import nwt.impala.LTMRequest;
import nwt.security.ApiAuthenticationEntryPoint;
import nwt.services.LTMDayService;


@RestController
@RequestMapping(value = "/ltm")
public class LTMDayController {
	
	@Autowired(required = true)
    private LTMDayService LTMService;
    
	private static final Logger logger = LoggerFactory.getLogger(ApiAuthenticationEntryPoint.class);
	
	@CrossOrigin	
	@RequestMapping(value = "/data", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<LTMDay>> getBS_LN_DIMByCode(@RequestBody LTMRequest ltmRequest){		
	return new ResponseEntity<List<LTMDay>>(LTMService.getByDeviceName(ltmRequest.getdeviceFilter()), HttpStatus.OK);
	}
		

}
