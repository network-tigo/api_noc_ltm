package nwt.services;

import com.mongodb.BasicDBObject;

import nwt.mongo.horus.AuthResponse;

public interface IAuthenticateService {
	AuthResponse authenticateUser(String user, String password);
	
	BasicDBObject validToken(String token);
}
