package nwt.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;


@Service 
public class CacheService {
	
	@Autowired
    private CacheManager cacheManager;
		
	public void cleanCache() {
		cacheManager.getCacheNames().parallelStream().forEach(name -> cacheManager.getCache(name).clear());
	}
	
	
}
