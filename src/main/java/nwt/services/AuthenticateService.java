package nwt.services;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.security.auth.login.AccountException;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;

import nwt.mongo.horus.AdminUser;
import nwt.mongo.horus.AuthResponse;
import nwt.repositories.horus.AdminUsersRepository;
import nwt.security.JwtTokenProvider;

@Service
public class AuthenticateService implements IAuthenticateService {
	
	@Autowired
	private AdminUsersRepository adminUserRepository;
	
	@Autowired
	private JwtTokenProvider jtp;

	private final String CONTEXT_FACTORY_CLASS = "com.sun.jndi.ldap.LdapCtxFactory";
	 
    private String ldapServerUrls[];
 
    private int lastLdapUrlIndex;
 
    private String domainName = "Comcel.com.gt";
    
    public AuthenticateService() {}
    
    public AuthenticateService(String domainName) {
        this.domainName = domainName.toUpperCase();
 
        try {
            ldapServerUrls = nsLookup(domainName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        lastLdapUrlIndex = 0;
    }

	@Override
	public AuthResponse authenticateUser(String username, String password) {
		String domain = "Comcel.com.gt";
		AuthenticateService authentication = new AuthenticateService(domain);
	    try {
	        boolean authResult = authentication.authenticate(username, password);
	        
	        // username = "lsuriano";
	        AdminUser user = getUser(username);
	        if(user != null) {
	        	//boolean hasG =  user.getGroups().stream().anyMatch( (s) -> s.startsWith("network_analytics") ) || user.getGroups().contains("super_admin"); 
	        	boolean hasG =  user.getGroups().stream().anyMatch( (s) -> (s.contains(" ")==false) ) || user.getGroups().contains("super_admin");

		        String tkn = jtp.generateToken(user);
		        
		        AuthResponse result = new AuthResponse();
		        if(authResult && hasG) {	        	
			        result.setUserName(username);
			        result.setName(user.getName());
			        result.setToken(tkn);
			        result.setPermissions(user.getGroups());
			        result.setPhoto(user.getPhoto());
			        
		        	return result;
		        }
		        else {
		        	return result;
		        } 
	        }
	        
	    } catch (LoginException e) {
	        e.printStackTrace();
	    }
	    AuthResponse resultFail = new AuthResponse();
	    return resultFail;
	}
	
	private boolean authenticate(String username, String password) throws LoginException {
		if (ldapServerUrls == null || ldapServerUrls.length == 0) {
            throw new AccountException("Unable to find ldap servers");
        }
        if (username == null || password == null || username.trim().length() == 0 || password.trim().length() == 0) {
            throw new FailedLoginException("Username or password is empty");
        }
        int retryCount = 0;
        int currentLdapUrlIndex = lastLdapUrlIndex;
        do {
            retryCount++;
            try {
                Hashtable<Object, Object> env = new Hashtable<Object, Object>();
                env.put(Context.INITIAL_CONTEXT_FACTORY, CONTEXT_FACTORY_CLASS);
                env.put(Context.PROVIDER_URL, ldapServerUrls[currentLdapUrlIndex]);
                env.put(Context.SECURITY_PRINCIPAL, username + "@" + domainName);
                env.put(Context.SECURITY_CREDENTIALS, password);
                DirContext ctx = new InitialDirContext(env);
                ctx.close();
                lastLdapUrlIndex = currentLdapUrlIndex;
                return true;
            } catch (CommunicationException exp) {
                //exp.printStackTrace();
                // if the exception of type communication we can assume the AD is not reachable hence retry can be attempted with next available AD
                if (retryCount < ldapServerUrls.length) {
                    currentLdapUrlIndex++;
                    if (currentLdapUrlIndex == ldapServerUrls.length) {
                        currentLdapUrlIndex = 0;
                    }
                    continue;
                }
                return false;
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                return false;
            }
        } while (true);
	}
	
	private static String[] nsLookup(String argDomain) throws Exception {
        try {
            Hashtable<Object, Object> env = new Hashtable<Object, Object>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.dns.DnsContextFactory");
            env.put("java.naming.provider.url", "dns:");
            DirContext ctx = new InitialDirContext(env);
            Attributes attributes = ctx.getAttributes(String.format("_ldap._tcp.%s", argDomain), new String[] { "srv" });
            // try thrice to get the KDC servers before throwing error
            for (int i = 0; i < 3; i++) {
                Attribute a = attributes.get("srv");
                if (a != null) {
                    List<String> domainServers = new ArrayList<String>();
                    NamingEnumeration<?> enumeration = a.getAll();
                    while (enumeration.hasMoreElements()) {
                        String srvAttr = (String) enumeration.next();
                        // the value are in space separated 0) priority 1)
                        // weight 2) port 3) server
                        String values[] = srvAttr.toString().split(" ");
                        domainServers.add(String.format("ldap://%s:%s", values[3], values[2]));
                    }
                    String domainServersArray[] = new String[domainServers.size()];
                    domainServers.toArray(domainServersArray);
                    return domainServersArray;
                }
            }
            throw new Exception("Unable to find srv attribute for the domain " + argDomain);
        } catch (NamingException exp) {
            throw new Exception("Error while performing nslookup. Root Cause: " + exp.getMessage(), exp);
        }
    }

	private AdminUser getUser(String username) {
		AdminUser user = adminUserRepository.getUserConsideringId(username);
		return user;
	}

	@Override
	public BasicDBObject validToken(String token) {
		
		boolean isValid = jtp.validateToken(token);
		BasicDBObject result = new BasicDBObject();
				
		if(isValid) {
			AdminUser userGot = jtp.getUserFromJWT(token);
			
			String level = null;
	        if(userGot.getGroups().contains("tigo_business_executive")) {
	        	level = "executive";
	        }
	        else if(userGot.getGroups().contains("tigo_business_coordinator")) {
	        	level = "coordinator";
	        }
	        else if(userGot.getGroups().contains("tigo_business_manager")) {
	        	level = "manager";
	        }
			
			result.append("authenticate", true);
	        result.append("username", userGot.getId());
	        result.append("name", userGot.getName());
	        result.append("token", token);
	        result.append("level", level);
		}
		else {
			result.append("authenticate", false);
	        result.append("username", null);
	        result.append("name", null);
	        result.append("token", null);
	        result.append("level", null);
	        result.append("q", null);
	        result.append("n", null);
	        result.append("p", null);
	        result.append("a", null);
		}
		
		return result;
	}

}
