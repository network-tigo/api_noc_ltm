package nwt.services;

import java.util.List;
import nwt.impala.LTMDay;
import nwt.impala.SitesDay;
import nwt.repositories.impala.LTMDayInfoRepo;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class LTMDayService implements IfaceLTMService {
	
	
	@Autowired
	LTMDayInfoRepo ltmRepository;
		
	@Override
	public List<LTMDay> getByDeviceName(String devicename) {
		return ltmRepository.findByDeviceName(devicename) ;
	}

}
	



