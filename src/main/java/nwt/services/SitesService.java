package nwt.services;

import java.util.List;
import nwt.impala.SitesDay;
import nwt.repositories.impala.SitesInfoRepo;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class SitesService implements IfaceSiteService {
	
	@Autowired
	SitesInfoRepo SitesRepository;
		
	@Override
	public List<SitesDay> SitesbyDeviceName(String devicename) {
		return SitesRepository.CgisbyDeviceName(devicename) ;
	}

}

	
