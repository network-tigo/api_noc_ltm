package nwt.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import com.fasterxml.jackson.databind.ObjectMapper;

import nwt.logger.CustomLogger;
import nwt.mongo.Error;
import nwt.mongo.ErrorResponse;
import nwt.security.ApiAuthenticationEntryPoint;

import  com.cloudera.impala.jdbc.Driver;

@Component
public class ApiAuthenticationEntryPoint implements AuthenticationEntryPoint {
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private CustomLogger customLogger;
	
	private static final Logger logger = LoggerFactory.getLogger(ApiAuthenticationEntryPoint.class);
	
	@Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
		long startTime = System.currentTimeMillis();
		logger.error("Responding with unauthorized error. Message - {}", e.getMessage());

		httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		httpServletResponse.setContentType("application/json");
		httpServletResponse.setCharacterEncoding("UTF-8");
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		Error error = new Error("SEG", messageSource.getMessage("unauth", new Object[] {}, new Locale("en")), "001", 401l);
		ErrorResponse er = new ErrorResponse(error);				
		
		ContentCachingRequestWrapper wrappedRequest = new ContentCachingRequestWrapper(httpServletRequest);
		ContentCachingResponseWrapper wrappedResponse = new ContentCachingResponseWrapper(httpServletResponse);
		
		ObjectMapper mapper = new ObjectMapper();
		String responseBody = mapper.writeValueAsString(er);
		
		try {
			customLogger.WriteToLog(wrappedRequest, wrappedResponse, startTime, responseBody);
		} catch (JSONException e1) {}
		
		PrintWriter writer = httpServletResponse.getWriter();
		writer.println(objectMapper.writeValueAsString(er));
    }

}
