package nwt.security;

import io.jsonwebtoken.*;
import nwt.mongo.horus.AdminUser;

import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.stereotype.Service;

@Service
public class JwtTokenProvider {
	
	private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Value("${app.jwtSecret}")
    private String jwtSecret;
    
    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;
    
    @Value("${app.jwtIssuer}")
    private String jwtIssuer;

	public String generateToken(AdminUser user) {
		// The JWT signature algorithm we will be using to sign the token
    	SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
    	
    	// We will sign our JWT with our ApiKey secret
    	byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(jwtSecret);
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
		
		Date now = new Date();
		Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);
		
		JwtBuilder builder = Jwts.builder()
				.setId(user.getId())
				.setSubject(user.getId())
				.setIssuedAt(now)
				.setIssuer(jwtIssuer)
				.setExpiration(expiryDate)
				.signWith(signatureAlgorithm, signingKey);
		
		builder.claim("username", user.getId());
		builder.claim("groups", user.getGroups());
		builder.claim("name", user.getName());
		builder.claim("phone", user.getPhone());
		builder.claim("area", user.getArea());
		
		return builder.compact();
	}
	
	public AdminUser getUserFromJWT(String token) {
    	Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(jwtSecret)).parseClaimsJws(token).getBody();
    	System.out.println(claims);
    	
    	String username = (String)claims.get("username");
    	List<String> groups = (ArrayList<String>)claims.get("groups");
    	String name = (String)claims.get("name");
    	String phone = (String)claims.get("phone");
    	String area = (String)claims.get("area");
    	
    	AdminUser user = new AdminUser();
    	user.setId(username);
    	user.setGroups((ArrayList<String>) groups);
    	user.setName(name);
    	user.setPassword(phone);
    	user.setArea(area);

    	return user;
    }
	
	public boolean validateToken(String authToken) {
        try {
            Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(jwtSecret)).parseClaimsJws(authToken).getBody();
            boolean isApiGee = false;
            
            if(claims.containsKey("api_gee")) {
            	isApiGee = (Boolean)claims.get("api_gee");
            }
            
            return isApiGee;
        } catch (SignatureException ex) {
            logger.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty.");
        }
        return false;
    }
}
