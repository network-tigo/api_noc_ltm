package nwt.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import com.fasterxml.jackson.databind.ObjectMapper;

import nwt.logger.CustomLogger;
import nwt.mongo.Error;
import nwt.mongo.ErrorResponse;
import nwt.mongo.GenericPrincipal;

public class AuthenticationFilter extends AbstractPreAuthenticatedProcessingFilter {
	
	private MessageSource messageSource;
	
	private CustomLogger customLogger;

    public AuthenticationFilter(MessageSource messageSource) {
        this.messageSource = messageSource;
        this.customLogger = new CustomLogger();
    }

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
    	String token = request.getHeader("Authorization");
    	
    	if(token == null)
    		return null;
    	else {
    		if(token.contains("Bearer "))
    			token = token.replace("Bearer ", "");
    		else
    			return null;
    	}
    	
    	GenericPrincipal principal = new GenericPrincipal();
    	principal.setUser("API Gee");
    	principal.setToken(token);
        return principal;
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        return "Authorization";
    }
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		long startTime = System.currentTimeMillis();
		
		ContentCachingRequestWrapper wrappedRequest = new ContentCachingRequestWrapper((HttpServletRequest) request);
		ContentCachingResponseWrapper wrappedResponse = new ContentCachingResponseWrapper((HttpServletResponse) response);
		
    	try {
    		super.doFilter(request, response,chain);
    	}
    	catch (DataAccessException e) {
			wrappedResponse.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
			wrappedResponse.setContentType("application/json");
			wrappedResponse.setCharacterEncoding("UTF-8");
			
			Error error = new Error("COM", messageSource.getMessage("service_unavailable", new Object[] {}, new Locale("en")), "002", 503l);
			ErrorResponse er = new ErrorResponse(error);
			ObjectMapper mapper = new ObjectMapper();
			PrintWriter writer = wrappedResponse.getWriter();
			
			String responseBody = mapper.writeValueAsString(er);
			try {
				customLogger.WriteToLog(wrappedRequest, wrappedResponse, startTime, responseBody);
			} catch (JSONException e1) {}
			
			writer.println(mapper.writeValueAsString(er));
			wrappedResponse.copyBodyToResponse();
		}
    }

}
