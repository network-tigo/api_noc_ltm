package nwt.impala;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name = "stage.st_ltm_wttx")
@Table(name = "stage.st_ltm_wttx", schema = "default")

public class LTMDay {

	@EmbeddedId
	private LTM ltmGeneralData;
	
	public LTMDay() {}
	
	public LTMDay(Integer prttnDt, String esn, String devicename, String softwareversion, String modelname, String ip, String sinr, String rsrp, String rsrq, String rssi, String cqi, String band, String tac, String power, Double totalupload, Double totaldownload, String ecgi, String maxdlthroughput, String maxulthroughput, Integer totalusernum, String imsi, String iccid, String collecttime, String ipaddress) {
		setltmGeneralData(new LTM( prttnDt,  esn, devicename,  softwareversion,  modelname,  ip,  sinr,  rsrp, rsrq, rssi,  cqi,  band,  tac,  power,  totalupload,  totaldownload,  ecgi,  maxdlthroughput,  maxulthroughput,  totalusernum,  imsi,  iccid, collecttime, ipaddress));
	}
	
	public LTM getltmGeneralData() {
		return ltmGeneralData;
	}
		
	public void setltmGeneralData(LTM ltmGeneralData) {
		this.ltmGeneralData = ltmGeneralData;
	}
	
}
