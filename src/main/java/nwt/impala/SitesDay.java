package nwt.impala;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name = "stage.st_ltm_wttx")
@Table(name = "stage.st_ltm_wttx", schema = "default")


public class SitesDay {
	
	@EmbeddedId
	private Sites infoSitesData;
	
	public SitesDay() {}
	
	public SitesDay(String esn, String devicename, String ecgi, String collecttime, String ipaddress, String imsi, String ip) {
		SetSitesDay(new Sites(esn,devicename,ecgi,collecttime, ipaddress, imsi, ip));
	}
	
	public Sites getSitesDay() {
		return infoSitesData;
	}
		
	public void SetSitesDay(Sites infoSitesData) {
		this.infoSitesData = infoSitesData;
	}

}
