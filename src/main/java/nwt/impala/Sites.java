package nwt.impala;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Embeddable
public class Sites   implements Serializable {

	@Column(name = "esn")
    private String esn;
 
	@Column(name = "devicename")
    private String devicename;
	
	@Column(name = "ecgi")
	private String ecgi;
	
	@Column(name = "collecttime")
    private String collecttime;
	
	@Column(name = "ipaddress")
    private String ipaddress;
	
	@Column(name = "imsi")
	private String imsi;
	
	@Column(name = "ip")
    private String ip;
	
	public Sites() {}
	
	public Sites(String esn, String devicename, String ecgi, String collecttime, String ipaddress, String imsi, String ip) {
		setEsn(esn);
		setDevicename(devicename);
		setEcgi(ecgi);
		setCollecttime(collecttime);
		setIpaddress(ipaddress);
		setImsi(imsi);
		setIp(ip);
		
	}
	
	public void setEsn(String esn) {
		 this.esn = esn;
	 } 
	
	public void setDevicename(String devicename) {
		 this.devicename = devicename;
	 } 
	
	public void setEcgi(String ecgi)
	 {
			this.ecgi = ecgi ; 
		 }
	
	
	public void setCollecttime(String collecttime)
	 {
			this.collecttime = collecttime ; 
		 }
	
	public void setIpaddress(String ipaddress)
	 {
			this.ipaddress = ipaddress ; 
		 }
	
	
	public void setImsi(String imsi)
	 {
			this.imsi = imsi ; 
		 }
	
	
	public void setIp(String ip)
	 {
			this.ip = ip ; 
		 }
	
	
	
	
	
	
	
	

	
	public String getEsn() {
		 return esn;
	 }
	 
	 public String getDevicename() {
		 return devicename;
	 } 
	 
	 public String getEcgi( )
	 {
			return ecgi ; 
		 }
	 
	 public String getCollecttime( )
	 {
			return collecttime ; 
		 }
	 
	 public String getIpaddress( )
	 {
			return ipaddress ; 
		 }
	 
	 public String getImsi( )
	 {
			return imsi ; 
		 }
	 
	 public String getIp( )
	 {
			return ip ; 
		 }
	 
	 
	
}
