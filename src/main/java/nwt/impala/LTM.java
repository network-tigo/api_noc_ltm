package nwt.impala;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Embeddable
public class LTM  implements Serializable{

	 @Column(name = "prttn_dt")
	    private Integer prttnDt;
	 
	 @Column(name = "esn")
	    private String esn;
	 
	 @Column(name = "devicename")
	    private String devicename;
	 
	 @Column(name = "softwareversion")
	    private String softwareversion;
	 
	 @Column(name = "modelname")
	    private String modelname;
	 
	 @Column(name = "ip")
	    private String ip;
	 
	 @Column(name = "sinr")
	    private String sinr;
	 
	 @Column(name = "rsrp")
	    private String rsrp;
	 
	 @Column(name = "rsrq")
	    private String rsrq;
	 
	 @Column(name = "rssi")
	    private String rssi;
	 
	 @Column(name = "cqi")
	    private String cqi;
	 
	 @Column(name = "band")
	    private String band;
	 
	 @Column(name = "tac")
	    private String tac;
	 
	 @Column(name = "power")
	    private String power;
	 
	 @Column(name = "totalupload")
	    private Double totalupload;
	 
	 @Column(name = "totaldownload")
	    private Double totaldownload;
	 
	 @Column(name = "ecgi")
	    private String ecgi;
	 
	 @Column(name = "maxdlthroughput")
	    private String maxdlthroughput;
	 
	 @Column(name = "maxulthroughput")
	    private String maxulthroughput;
	 
	 @Column(name = "totalusernum")
	    private Integer totalusernum;
	 
	 @Column(name = "imsi")
	    private String imsi;
	 
	 @Column(name = "iccid")
	    private String iccid;
	 
	 @Column(name = "collecttime")
	    private String collecttime;
	 
	 @Column(name = "ipaddress")
	    private String ipaddress;
	 
	 public LTM() {}
	 
	 public LTM(Integer prttnDt, String esn,String devicename, String softwareversion, String modelname, String ip, String sinr, String rsrp, String rsrq, String rssi, String cqi, String band, String tac, String power, Double totalupload, Double totaldownload, String ecgi, String maxdlthroughput, String maxulthroughput, Integer totalusernum, String imsi, String iccid, String collecttime,String ipaddress) {
	    	setPrttnDt(prttnDt);
	    	setEsn(esn);
	    	setDevicename(devicename);
	    	setSoftwareversion(softwareversion); 
	    	setModelname(modelname);
            setIp(ip); 
            setSinr(sinr);
            setRsrp(rsrp);
            setRsrq(rsrq);
            setRssi(rssi); 
            setCqi(cqi); 
            setBand(band); 
            setTac(tac); 
            setPower(power);
            setTotalupload(totalupload); 
            setTodaldownload(totaldownload); 
            setEcgi(ecgi);
            setMaxdlThp(maxdlthroughput);
            setMaxulThp(maxulthroughput);
            setTotalusernum(totalusernum);
            setImsi(imsi); 
            setIccid(iccid);
            setCollecttime(collecttime);
            setIpaddress(ipaddress);
	    	
	    }
	 
	 public void setPrttnDt(Integer prttnDt){
	        this.prttnDt = prttnDt;
	    }
	 
	 public void setEsn(String esn) {
		 this.esn = esn;
	 } 
	 
	 public void setDevicename(String devicename) {
		 this.devicename = devicename;
	 } 
 	
	 public void setSoftwareversion(String softwareversion) {
		this.softwareversion = softwareversion; 
	 }
 	
	 public void setModelname(String modelname)
	 {
			this.modelname = modelname ; 
		 }
	 
	 
	 public void setIp(String ip)
	 {
			this.ip = ip ; 
		 }
	 
	 public void setSinr(String sinr)
	 {
			this.sinr = sinr ; 
		 }
	 
	 public void setRsrp(String rsrp)
	 {
			this.rsrp = rsrp ; 
		 }
	 
	 public void setRsrq(String rsrq)
	 {
			this.rsrq = rsrq ; 
		 }
	 
	 public void setRssi(String rssi)
	 {
			this.rssi = rssi ; 
		 }
	 
	 public void setCqi(String cqi)
	 {
			this.cqi = cqi ; 
		 }
	 
	 public void setBand(String band)
	 {
			this.band = band ; 
		 }
	 
	 public void setTac(String tac)
	 {
			this.tac = tac ; 
		 }
	 
	 public void setPower(String power)
	 {
			this.power = power ; 
		 }
	 
	 public void setTotalupload(Double totalupload)
	 {
			this.totalupload = totalupload ; 
		 }
	 
	 public void setTodaldownload(Double totaldownload)
	 {
			this.totaldownload = totaldownload; 
		 }
			 
	 public void setEcgi(String ecgi)
	 {
			this.ecgi = ecgi ; 
		 }
	 
	 public void setMaxdlThp(String maxdlthroughput)
	 {
			this.maxdlthroughput = maxdlthroughput ; 
		 }
	 
	 public void setMaxulThp(String maxulthroughput)
			 {
			this.maxulthroughput = maxulthroughput; 
		 }
	 
	 public void setTotalusernum(Integer totalusernum)
	 {
			this.totalusernum = totalusernum; 
		 }
	 
	 public void setImsi(String imsi)
	 {
			this.imsi = imsi ; 
		 }
	 
	 public void setIccid(String iccid)
	 {
			this.iccid = iccid; 
		 }
	 
	 public void setCollecttime(String collecttime)
	 {
			this.collecttime = collecttime; 
		 }
	 
	 
	 public void setIpaddress(String ipaddress)
	 {
			this.ipaddress = ipaddress ; 
		 }
	 
	 
	 
	 
	 
	 public Integer getPrttnDt(){
	        return prttnDt;
	    }
	 
	 public String getEsn() {
		 return esn;
	 }
	 
	 public String getDevicename() {
		 return devicename;
	 } 
	
	 public String getSoftwareversion() {
		return softwareversion; 
	 }
	
	 public String getModelname()
	 {
			return modelname ; 
		 }
	 
	 
	 public String getIp()
	 {
			return ip ; 
		 }
	 
	 public String getSinr( )
	 {
			return sinr ; 
		 }
	 
	 public String getRsrp( )
	 {
			return rsrp  ; 
		 }
	 
	 public String getRsrq( )
	 {
			return rsrq  ; 
		 }
	 
	 public String getRssi( )
	 {
			return rssi  ; 
		 }
	 
	 public String getCqi()
	 {
			return cqi  ; 
		 }
	 
	 public String getBand( )
	 {
			return band  ; 
		 }
	 
	 public String getTac()
	 {
			return tac   ; 
		 }
	 
	 public String getPower( )
	 {
			return power ; 
		 }
	 
	 public Double getTotalupload( )
	 {
			return totalupload ; 
		 }
	 
	 public Double getTodaldownload( )
	 {
			return totaldownload  ; 
		 }
			 
	 public String getEcgi( )
	 {
			return ecgi ; 
		 }
	 
	 public String getMaxdlThp( )
	 {
			return maxdlthroughput  ; 
		 }
	 
	 public String getMaxulThp( )
			 {
			return maxulthroughput ; 
		 }
	 
	 public Integer getTotalusernum( )
	 {
			return totalusernum ; 
		 }
	 
	 public String getImsi()
	 {
			return imsi   ; 
		 }
	 
	 public String getIccid( )
	 {
			return   iccid; 
		 }
	 
	 public String getCollecttime()
	 {
		 return collecttime;
	 }
	 
	 public String getIpaddress()
	 {
			return ipaddress ; 
		 }
	 
	
}


